# Mon premier dépôt Git
Ceci est mon premier dépôt.

## Liste des commandes
- `git init` : initialise le dépôt
- `git add` : ajoute un fichier à a zone d'index
- `git commit` : valide les modifications indexées dans la zone d'index
-  `git status` : voir l'état du dépôt 
-  `git log` : voir l'historique  
-  `git commit -a` : ajoute au commit les fichiers modifiés (sans qu'ils
soient indexés).
-  `git commit -m` : permet d'ajouter le message de commit directement en une
seule commande, sans passer par l'éditeur temporaire.
-  `git commit -amend` : Ceci permet de modifier le dernier
commit, et ouvre l'éditeur temporaire pour éventuellement
modifier le message.
-  `git remote add  ...`  "mon_depot_distant git@gitlab.com:[votre_login]/depot-test-distant": pour ajouter le dépôt GitLab depot-test-distant comme remote
avec le nom mon_depot_distant.
(-  `git fetch` :  récupèrer une branche amont qui existe déja sur le serveur distant)
-  `git branch master --set-upstream-to= "nom depot distant/master` : pour que la branche local master suive automatiquement la branche master du remote "nom_depot_distant"
-  `git push` :  permet d'envoyer les modification de depot local au dépot distant 
                          faire "git push nom_depot_distant master"
-  `git pull` :  permet d'envoyer les modification de depot distant au dépot local 
- `git clone [lien SSH ou HTTPS]` : permet de cloner un dépôt distant en local                           
- `git tag v0 [SHA-1 du commit]` : créer une étiquette sur le commit  Rappel : on trouve le SHA-1 d'un
commit avec git log. Pour mettre une étiquette sur le dernier commit pas besoin de donner le SHA_1
-  `git pull/pull --tags`  : pour transfèrer les tags
